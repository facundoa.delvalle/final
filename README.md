Fecha: 13/12/2021
Nombre: Facundo Ariel
Apellido: del Valle
Materia: Programacion en bash
Profesor: Sergio Pernas
------------------------------

Este script es para facilitar la administración en el caso de
trabajo remoto, para verificar las conexiones a la maquina virtual y
comunicar de una manera mas simple.
 Este esta enfocado a administradores con permisos de root.
------------------------------

Explicación basica del script:
El script se divide en 2 partes para facilitar su lectura, el primero
"final.sh" que es el ejecutable, el cual tiene el lobby de uso y el
envio de mensajes a todas las terminales, ya que es un script corto.
El resto se llaman desde el fichero "funciones.sh".
 Este tiene la mayoria de scripts divididos en funciones.
------------------------------

Script de monitoreo:
 Este se maneja con 2 ficheros para compararlos entre si, para
verificar los usuarios que se conectan y desconectan a traves del
comando who que muestra las terminales activas utilizando el awk
para ordenarlo de una manera mas limpia.
 Para mostrar en pantalla el usuario desconectado se utilizo
el comando grep con la opcion -v para cambiar la selección,
por lo tanto se utiliza para comparar el fichero nuevo con el viejo y
viceversa con lo cual borra los usuarios que siguen activos y solo
deja en pantalla los usuarios que se conectan y desconectan.
 Esto se lee a traves de un while que corre constantemente, y cuando
un if interno detecta un cambio en logs da el aviso y luego actualiza
los mismos.
------------------------------

Script de busqueda:
 Este busca con un grep dentro del comando who, para ver
si esta conectado o no se compara el primer campo de la ip, si es
mayor a 1 entra en el if y da los datos, en el caso que no, es porque
no existe ese campo, por lo tanto no esta conectado, entronces no
entra en el if y da el aviso que esta desconectado.
------------------------------

Script de mensajes a terminales:
 Este busca en el who cual es la terminal que corresponde a
el usuario que escribe el usuario, luego se hace un echo
con el mensaje y se direcciona a la terminal en cuestion.
------------------------------

Script de mensajes de broadcast:
 este hace un echo del mensaje deseado por el usuario y
lo comparte a todos a traves del comando wall.
