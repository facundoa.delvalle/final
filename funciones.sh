#!/bin/bash

#directorios y permisos
touch $HOME/usrconnect
chmod 777 $HOME/usrconnect
touch $HOME/n_usrconnect
chmod 777 $HOME/n_usrconnect


#--------------------------------------------------------
#envio de mensajes a terminales desde root
function mensaje_pers(){
#comprobar si esta como usuario root

if [ $UID -ne 0 ]; then
        echo "esta opcion es solo para usuarios con permisos de root."
sleep 2
clear
lobby
fi

echo "¿a que usuario desea enviarle el mensajes?"
read USR
echo "escriba el mensaje que desea enviarle"
read MSJ
TTY=$(who | grep $USR | awk '{print $2}')
echo $TTY
echo $MSJ >> /dev/$TTY
}




#--------------------------------------------------------
#busqueda de inicios de sesion por nombre

function busqueda(){
echo 'escriba el usuario que desea ver si esta conectado'
read NAME
who | grep '$NAME'
COND=$(who | grep $NAME | awk 'BEGIN{FS="."}{print $2}' | head -1)
clear
if [[ $COND -gt 1 ]]; then
IP=$(who | grep $NAME | awk '{print $5}')
echo 'el usuario:' $NAME 'esta conectado a traves de la ip:' $IP
else
echo 'el usuario:' $NAME 'no se encuentra conectado'
fi
}

#----------------------------------------------------
#monitor para control de personal, aviso de inicio y corte de sesion

function monitoreo(){
echo '================================';
echo '= presione CTRL + c para salir =';
echo '================================';
who | awk '{print $2" "$4" "$5" "$1}' > $HOME/usrconnect
VISIBLE=$(cat $HOME/usrconnect )
NVISIBLE=$(wc -l <<< "${VISIBLE}")
CONECTADOS=$(who | awk '{print $2" "$4" "$5" "$1}')
NCONECTADOS=$(wc -l <<< "${CONECTADOS}")
who | awk '{print $2" "$4" "$5" "$1}' > $HOME/n_usrconnect

while [[ : ]];do
CONECTADOS=$(who | awk '{print $2" "$4" "$5" "$1}')
NCONECTADOS=$(wc -l <<< "${CONECTADOS}")
who | awk '{print $2" "$4" "$5" "$1}' > $HOME/n_usrconnect

if [[ $NVISIBLE -gt $NCONECTADOS ]];then
echo "se desconecto el usuario"
grep -vf "$HOME/n_usrconnect" $HOME/usrconnect
echo "-----------------------------------"
who | awk '{print $2" "$4" "$5" "$1}' > $HOME/usrconnect
VISIBLE=$(cat $HOME/usrconnect )
NVISIBLE=$(wc -l <<< "${VISIBLE}")
sleep 1
fi

if [[ $NVISIBLE -lt $NCONECTADOS ]];then
echo "se conecto el usuario"
grep -vf "$HOME/usrconnect" $HOME/n_usrconnect
echo "-----------------------------------"
who | awk '{print $2" "$4" "$5" "$1}' > $HOME/usrconnect
VISIBLE=$(cat $HOME/usrconnect )
NVISIBLE=$(wc -l <<< "${VISIBLE}")
sleep 1
fi

sleep 2
done
}
