#!/bin/bash

source funciones.sh

#lobby del programa
function lobby(){
        echo 'bienvenido' $USER;
        echo 'elige la opción deseada';

        echo '====================================================================';
        echo '= 1) buscar si un usuario esta conectado                           =';
        echo '= 2) monitorear inicios y cierres de sesion                        =';
        echo '= 3) enviar mensajes a todas las terminales                        =';
        echo '= 4) enviar mensajes a otra terminal (solo con permisos de root)   =';
        echo '= 0) salir del programa                                            =';
        echo '====================================================================';

read A
        case $A in
#buscar si un usuario esta conectado
        1) clear
           busqueda
           sleep 2
           clear
;;
#monitor de inicios y cierres de sesion
        2) clear
           monitoreo
;;
#enviar mensajes a todas las terminales
        3) clear
           echo "escriba el mensaje que desea que se envie a todos"
           read MSJ
           echo $MSJ | wall
           sleep 2
           clear
;;
#enviar mensaje a una terminal
        4) clear
           mensaje_pers
           sleep 2
           clear
;;
#salir del programa
        0) clear
           echo "hasta luego" $USER
           sleep 2
           clear
           exit
        esac
}
while [ : ];
do
lobby
done
